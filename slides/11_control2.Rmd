---
title: Heterogeneous treatment effects
author: Tom Parker
date: \today
output:
  beamer_presentation:
    slide_level: 2
    includes:
      in_header: custom.tex
fontsize: 11pt
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(cache=TRUE, scipen = 2, digits = 4)
options(scipen = 2, digits = 4)
```

```{r seed, include=FALSE, cache=TRUE}
set.seed(888)
knitr::opts_chunk$set(cache.extra = knitr::rand_seed)
```

# Heterogeneous treatment effects for experimental data

## A model for heterogeneous treatment effects
- Treatment effects don't really need to be the same for all individuals.
- While we've seen that it is not possible to measure $Y_{1i} - Y_{0i}$ for any
  specific unit $i$, maybe we can allow for the effect to be indexed by
  covariates.
- In general this would mean that we have a model like
  \begin{equation*}
    \ex{Y_i | D_i, X_i} = \alpha(X_i) D_i + X_i\tr \beta,
  \end{equation*}
  but that is usually too flexible.
- We will parameterize our effect to make it tractable: suppose that
  \begin{equation*}
    \ex{Y_i | D_i, X_i} = \alpha D_i + X_i\tr \beta + (D_i \times X_i)\tr
    \gamma.
  \end{equation*}

## Model-based HTE definition
- The model
  \begin{equation*}
    \ex{Y_i | D_i, X_i} = \alpha D_i + X_i\tr \beta + (D_i \times X_i)\tr
    \gamma
  \end{equation*}
  is convenient because we can easily estimate it with linear regression.
  - Just add interactions of $D$ with whichever $X$ you think are important (or
    probably with all of them).
  - If $D_i$ is truly randomized so that it is independent from $X_i$, then you
    wouldn't need to include $X_i\tr \beta$ in estimation, but it's good
    practice in case of misspecification.
- Then the HTE, for individuals with $X_i = x$, for treatment $B$ with $d_i = b$
  vs treatment $A$ with $d_i = a$ is
  \begin{align*}
    HTE_i = HTE_i(x) &= \ex{Y_i | D_i = b, X_i = x} - \ex{Y_i | D_i = a, X_i = x} \\
    {} &= \alpha b + (b \times x)\tr \gamma - \alpha a - (a \times x)\tr \gamma
    \\
    {} &= (\alpha + x\tr \gamma) (b - a).
  \end{align*}

## The Oregon Health Insurance experiment revisited
- Recall that the OHIE randomly assigned people access to Medicaid.  We could
  look at how this varies for different sub-populations of the Oregonians who
  were affected by this plan.
- Recall that randomization wasn't perfect because people in larger households
  tended to get enrollment offers with higher probability.  We control for this
  effect in the baseline estimate:

\scriptsize
```{r ohie_basic}
load("../data/lecture_examples/OR_clean.RData") # Loads data.frame P
mod_basic <- lm(doc_any_12m ~ selected + numhh, data = P)
coef(summary(mod_basic))["selected", ]
```
\normalsize

- The ATE of an enrollment offer is about a 6% increase in seeing the doctor.

## OHIE with more covariates
- There is much more data available to us from the Oregon experiment.
- The code for preparing the OHIE data has been lengthened to include more
  covariates (and also response variables).
  - Use the new `ohie_prepare.R` or download the new `OR_clean.RData` to follow
    along.
- The new covariates are included in a data frame called `X`.  We should discuss
  how they were constructed.

## Aside: missing data
- What do we do when there are (partially) missing observations?
  - This occurs regularly with survey data like the OHIE survey.
- This is a big area, but here are two simple things to do and to keep in mind
  when data may be high-dimensional and sparse.
- We will to **impute** the values of missing covariates.  That is, we will make
  a simple guess of what their values should be, and fill in the missing parts
  with our guesses.
- For categorical variables it's safest to make a new category for the missing
  entries.
- For numeric variables the simplest imputation is to use the mean of the
  variable in place of missing variables.  That guarantees not to do anything
  strange to the results (but it could shrink their apparent importance).
- However, in case the data are sparse, replacing missing entries with means
  would add lots of very small nonzero numbers.  The benefits of sparsity are
  probably bigger than the benefits of having the mean in place of missing
  variables.

## Imputation example in R for numeric variables
- `impute()` follows a simple rule to impute missing variables.
- If more than half of the entries are zero, impute zeros to the missing parts.
  Otherwise impute the mean.

\scriptsize
```{r imp_fcn, eval=FALSE}
impute <- function(v) {
  if(mean(v == 0, na.rm = TRUE) > 0.5) impt <- 0
  else impt <- mean(v, na.rm = TRUE)
  v[is.na(v)] <- impt
  return(v)
}
# Then you can use apply() on an appropriately prepared matrix:
X_imputed <- apply(X, 2, impute)
```
\normalsize

## Imputation example in R for categorical variables
- Imputing categorical variables is easy if you have already created a new
  baseline level: make the missing data a new category.
- The function `naref()` applies `xnaref()` by column to a data frame.

\scriptsize
```{r naref_fcn, eval=FALSE}
xnaref <- function(x) {
  if (is.factor(x)) {
    if (!is.na(levels(x)[1])) {
      x <- factor(x, levels = c(NA, levels(x)), exclude = NULL)
    }
  }
  x
}

naref <- function(X) {
  if (is.null(dim(X))) return(xnaref(X))
  if (!is.data.frame(X)) stop ("Argument should be data frame or factor")
  X <- lapply(X, xnaref)
  as.data.frame(X)
}
```
\normalsize

## Interactive treatment effects for the OHIE data
- In `ohie_prepare.R`, missing values for new variables are prepared and put in
  a data frame called `X`.
- Now we can add them to `numhh` and interact everything with the treatment
  variable `selected`.

\scriptsize
```{r ohie_int_prep}
library(glmnet) # also loads Matrix library, which has sparse.model.matrix().
xhte <- sparse.model.matrix(~ ., data = cbind(numhh = P$numhh, X))[, -1]
dim(xhte)
dxhte <- P$selected * xhte
colnames(dxhte) <- paste("d", colnames(xhte), sep = ".")
htedesign <- cbind(xhte, d = P$selected, dxhte)
```
\normalsize

- `xhte` has `r ncol(xhte)` columns, and `htedesign` has `r ncol(htedesign)`
  columns.

## Estimation with the lasso
- Recall that the "access" treatment came from a truly randomized experiment.
  So we don't need to estimate the treatment assignment equation.
- We can estimate the baseline controls without penalty to ensure that we
  estimate their effect, and let the lasso search for the important
  interactions.

\scriptsize
```{r ohie_lasso}
wvec <- rep(1, ncol(htedesign))
wind <- which(dimnames(htedesign)[[2]] %in% c("numhh2", "numhh3+", "d"))
wvec[wind] <- 0 # Now the above three varaibles will have no penalty
htefit <- cv.glmnet(htedesign, P$doc_any_12m, penalty.factor = wvec)
hte <- coef(htefit, s = "lambda.min")[-(1:(ncol(xhte) + 1)), ]
```
\normalsize

## Heterogeneous treatment effects
- What do we find?

\scriptsize
```{r hte_est}
sort(hte, decreasing = TRUE)[1:6]
sort(hte)[1:6]
hte["d"] + colMeans(xhte) %*% hte[grep("d.", names(hte))]
```
\normalsize

# Heterogeneous treatment effects for observational data

## Consumer demand
- The last example estimated the heterogeneous effects of randomized access on
  health outcomes.
- However, for observational data we need to use the orthogonal machine learning
  approach to estimate conditional treatment effects.
- Here we will look at local price elasticities (i.e., due to small price
  fluctuations) for beer sold in supermarkets.
  - Of natural importance in business.  For example, in a constant price
    elasticity of demand model with elasticity $\alpha < -1$ and marginal cost
    $c$, a firm maximizes marginal revenue by setting $p = \alpha / (1 + \alpha)
    c$.
- Machine learning tools can be fooled without a model similar to the LTE model.
  - e.g., hotel room rates around the holidays, if you have no model of consumer
    demand.

## Demand for beer: general considerations
- There may be many factors that influence a consumer's decision to buy a good.
- To assert conditional independence in the LTE model, we need to know what
  variables might affect both prices and quantities demanded.
- Assuming that supply is fairly stable, the important variables are those
  "demand signals" that can be known to the price setter.
  - If something makes a person buy beer that the store cannot know about, that
    belongs in an error term.
- Local store owners can probably account for all of those signals.  If they do,
  then the left-over parts are conditionally ignorable.

## A linear model for demand
- Now we put those considerations in the form of a linear treatment effects
  model.
- Those demand signals that we think stores know are $X$ in the model
  \begin{align*}
    \log Q_{it} &= \alpha \log P_{it} + X_{it}\tr \beta + U_{it} \\
    \log P_{it} &= X_{it}\tr \gamma + V_{it}.
  \end{align*}
  - The data will have stores (indexed by $i$) and weeks (indexed by $t$).
- If $X$ is properly accounted for, then we can assume conditional
  ignorability to finish off our model:
  \begin{equation*}
    \ex{U_{it} | P_{it}, X_{it}} = 0, \quad \ex{V_{it} | X_{it}} = 0.
  \end{equation*}
- This model can be estimated using orthogonal machine learning methods.

## Beer in Chicago
- The data consist of beer sales data for Dominick's grocery stores in the
  Chicagoland area between 1989 and 1994.
- The data are available from the Kilts Center for Marketing at [this
  site](https://www.chicagobooth.edu/research/kilts/datasets/dominicks).
- We get weekly total unit sales and average prices for 287 unique product codes
  (UPCs) from 63 different grocery stores.  The whole dataset consist of 1.6
  million UPC/store/week observations.
- Here we will look at a subsample of 5000 observations and see how well that
  predicts behaviour in the larger dataset.

## What do we know?
- The data have been put in a file called `dominicks_beer.RData`.  It has three
  data frames, called `wber`, `upc` and `demo`.
- `wber` is pretty self-explanatory:

\scriptsize
```{r wber}
load("../data/beer/dominicks_beer.RData")
head(wber)
dim(wber)
sapply(wber, class)
```
\normalsize

- Store/week data, `UPC` is product, `PRICE` and `MOVE` are price and quantity
  sold.

## What do we know (2)?
- We also have a UPC file that has the UPCs as row names.  Otherwise there are
  descriptions of each product and the total quantity of beer in ounces in the
  whole package (cans/bottles all added up).
- Only the most common product formats have been included, take a look at the
  raw data if you want to investigate the others (also try `table(wber$UPC)` to
  see how many purchases of each UPC there are).

\scriptsize
```{r upc}
head(upc, 3)
dim(upc)
```
\normalsize

- What else?  `demo` has demographic summaries of the stores' neighbourhoods.

## Estimate a baseline model
- First create a "log price per 12 ounces" variable.
- Then take out 5000 observations and estimate a simple model.

\scriptsize
```{r mod}
#set.seed(888)
wber$lp <- log(12 * wber$PRICE / upc[wber$UPC, "OZ"])
ss <- sample.int(nrow(wber), 5e3)
margfit <- lm(log(MOVE) ~ lp, data = wber[ss, ])
coef(margfit)
```
\normalsize

- So sales drop by `r coef(margfit)[2]`% per 1% increase in price?  Is beer
  really that inelastic?
  - Would only really happen if all the products were very underpriced.  Or if
    we did a poor job estimating elasticities!

## Adding some covariates
- First, we could imagine that each store has its own mini-demand function, and
  we can do the same for type of beer and weeks:

\scriptsize
```{r fe}
wber$s <- factor(wber$STORE)
wber$u <- factor(wber$UPC)
wber$w <- factor(wber$WEEK)
xs <- sparse.model.matrix( ~ s - 1, data = wber)
xu <- sparse.model.matrix( ~ u - 1, data = wber)
xw <- sparse.model.matrix( ~ w - 1, data = wber)
```
\normalsize

## Add some more covariates!
- It's lazy of us to just add intercepts for everything, and that can lead to
  problems.
- In particular, similar products probably behave similarly, like things that
  are labeled "Miller", "lite" and "Miller Lite".
- Marketing professionals have models for *hierarchical* data structures like
  this, but we will take the easy way out: let's use the words in the product
  descriptions.
  - This will be dealt with in more detail in an upcoming week.

## The bag of words
- A **bag of words** model pulls words apart into **tokens** and makes
  indicators for each token.  In R the `tm` library (for *text mining*) will do
  some quick bag-of-words processing for us:

\scriptsize
```{r bow, messages=FALSE}
library(tm)
descr <- Corpus(VectorSource(as.character(upc$DESCRIP)))
descr <- DocumentTermMatrix(descr)
descr <- sparseMatrix(i = descr$i, j = descr$j, x = as.numeric(descr$v > 0),
          dims = dim(descr), dimnames = list(rownames(upc), colnames(descr)))
descr[1:3, 1:6]
descr[287, descr[287, ] != 0]
```
\normalsize

## Naive lasso with a lot of covariates
- Let's put all the covariates together in an object called `controls` and
  estimate a model of sales on `controls` with the lasso.

\scriptsize
```{r naive}
controls <- cbind(xs, xu, xw, descr[wber$UPC, ])
naivefit <- cv.glmnet(x = cbind(lp = wber$lp, controls)[ss, ],
                      y = log(wber$MOVE)[ss],
                      penalty.factor = c(0, rep(1, ncol(controls))),
                      standardize = FALSE)
print( coef(naivefit, s = "lambda.min")["lp", ] )
```
\normalsize

- The elasticity is now a little more believable.
- `standardize = FALSE` makes it so that variance weighting is not used: that
  would lead to a large penalty for beers that are bought often and a small
  one for those bought a little (mostly zeros).

## Orthogonal ML
- Naive lasso estimation is not really appropriate for finding treatment
  effects.  Instead, we should use the orthogonal ML methods from the last
  lecture.
- Recall that the `orthoLTE()` defined last time needed a "$D$" model
  estimator and a "$Y$" estimator.

\scriptsize
```{r ortho}
dreg <- function(x, d) {
  cv.glmnet(x, d, standardize = FALSE, lambda.min.ratio = 1e-5)
}
yreg <- function(x,y) {
  cv.glmnet(x, y, standardize = FALSE, lambda.min.ratio = 1e-5)
}
```
\normalsize

## `orthoLTE()`, lightly edited

\scriptsize
```{r olte}
require(sandwich)
orthoLTE <- function(x, d, y, dreg, yreg, nfold = 2) {
  nobs <- nrow(x)
  foldid <- sample(cut(1:nobs, breaks = nfold, labels = FALSE))
  subsam <- split(1:nobs, foldid)
  ytil <- dtil <- rep(NA, nobs)
  cat("fold: ")
  for(b in 1:length(subsam)) {
    dfit <- dreg(x[-subsam[[b]], ], d[-subsam[[b]]])
    yfit <- yreg(x[-subsam[[b]], ], y[-subsam[[b]]])
    dhat <- predict(dfit, x[subsam[[b]], ], s = "lambda.min", type = "response")
    yhat <- predict(yfit, x[subsam[[b]], ], s = "lambda.min", type = "response")
    dtil[subsam[[b]]] <- drop(d[subsam[[b]]] - dhat)
    ytil[subsam[[b]]] <- drop(y[subsam[[b]]] - yhat)
    cat(b, " ")
  }
  rfit <- lm(ytil ~ dtil)
  al <- coef(rfit)[2]
  se <- sqrt(vcovHC(rfit)[2, 2])
  cat(sprintf("\nalpha (se) = %g (%g)\n", al, se))
  return( list(alpha = al, se = se, dtil = dtil, ytil = ytil) )
}
```
\normalsize

## Orthogonal ML estimate
- What do we get from orthogonal ML?

\scriptsize
```{r dmlest}
orth <- orthoLTE(x = controls[ss, ], d = wber$lp[ss], y = log(wber$MOVE)[ss],
                  dreg = dreg, yreg = yreg, nfold = 5)
```
\normalsize

## The truth
- Let's find out what "the truth" is by estimating the big model with *all* the
  data, not just the subsample of 5000 observations.  That will be used as a
  benchmark.

\scriptsize
```{r mle_big}
fullfit <- glmnet(x = cbind(lp = wber$lp, controls), y = log(wber$MOVE),
                  lambda = 0)
coef(fullfit)["lp", ]
```
\normalsize

- Orthogonal MLE is not too far off (it's even inside a CI from the orthogonal
  ML estimate).

\scriptsize
```{r oCI}
orth$alpha + c(-2, 2) * orth$se
```
\normalsize

## Heterogeneous treatment effects: setup
- Now we will investigate how the elasticities change with other covariates.
- We need to estimate a model with the price and interactions of price and
  the $X$ variables of interest.
- Here, price is interacted with all the tokens from the bag of words to make
  `d`:

\scriptsize
```{r int}
xhte <- cbind(BASELINE = 1, descr[wber$UPC, ])
d <- xhte * wber$lp
colnames(d) <- paste("lp", colnames(d), sep = ":")
```
\normalsize

- Now `lp:BASELINE` is the vanilla price elasticity estimate and the others show
  modifications by product.

## "True" elasticities for a HTE model
- First, use the full sample to make heterogeneous elasticity estimates.

\scriptsize
```{r het_true}
# Matrix of features by unique UPC
eachbeer <- xhte[match(rownames(upc), wber$UPC), ]
rownames(eachbeer) <- rownames(upc)
# MLE with full sample
fullhte <- glmnet(x = cbind(d, controls), y = log(wber$MOVE), lambda = 0)
full_el <- drop(eachbeer %*% coef(fullhte)[2:(ncol(d)+1), ])
head(sort(full_el))
```
\normalsize

## Estimates using MLE and naive lasso
- Using the `ss` subsample, then estimating elasticities:

\scriptsize
```{r two_est}
mlehte <- glmnet(x = cbind(d, controls)[ss, ], y = log(wber$MOVE)[ss],
                  lambda = 0)
naivehte <- cv.glmnet(x = cbind(d, controls)[ss, ], y = log(wber$MOVE)[ss],
                      penalty.factor = c(0, rep(1, ncol(d) + ncol(controls) - 1)),
                      standardize = FALSE)
mle_el <- drop(eachbeer %*% coef(mlehte)[2:(ncol(d)+1), ])
naive_el <- drop(eachbeer %*% coef(naivehte, s = "lambda.min")[2:(ncol(d)+1), ])
head(sort(mle_el))
head(sort(naive_el))
```
\normalsize

## Estimate using orthogonal ML
- To estimate the model using orth. ML, recall that earlier we used the method
  to estimate residuals of $D$ given `controls`, which were effects for store,
  week, UPC and tokens.
- If we believe in the conditional ignorability of $D$ given those controls,
  then we could just use the residuals $\tilde{D}$ and $\tilde{Y}$ from the
  earlier ML step and estimate
  \begin{equation*}
    \ex{\tilde{Y}_i | \tilde{D}_i, X_i} = \alpha \tilde{D}_i + (\tilde{D}_i
    \times X_i)\tr \beta.
  \end{equation*}
- Then treatment effects are $\hat{\alpha} + X_i\tr \hat{\beta}$ for each $i$.

## The orthogonal ML HTE estimate
- Here is the estimate, using `orth` which was constructed earlier.
- Note that now we multiply `xhte` by `orth$dtil`, not by `wber$lp` as before
  with the MLE and naive lasso.

\scriptsize
```{r dml_est}
dmlhte <- cv.glmnet(x = xhte[ss, ] * orth$dtil, y = orth$ytil,
                    penalty.factor = c(0, rep(1, ncol(xhte) - 1)),
                    standardize = FALSE)
dml_el <- drop(eachbeer %*% coef(dmlhte, s = "lambda.min")[-1, ])
```
\normalsize

- This (second) estimate replaces OLS in the double ML algorithm with a lasso.

## Results: elasticity histograms (code)

\scriptsize
```{r hist_code, eval=FALSE}
library(colorspace)
pal <- sequential_hcl(4, palette = "hawaii")
par(mfrow = c(2, 2), mar = c(3.1, 3.1, 1.1, 1.1), mgp = 2:0)
hist(full_el, main = "all data", xlab = "elasticity",
      col = pal[1], breaks = "FD", freq = FALSE)
hist(mle_el, main = "mle", xlab = "elasticity",
      col = pal[2], breaks = 200, xlim = c(-25, 25), freq = FALSE)
hist(naive_el, main = "naive lasso", xlab = "elasticity",
      col = pal[3], freq = FALSE)
hist(dml_el, main = "orth. ML", xlab = "elasticity",
      col = pal[4], freq = FALSE)
```
\normalsize

## Results: elasticity histograms

\scriptsize
```{r hist_plot, echo=FALSE}
library(colorspace)
pal <- sequential_hcl(4, palette = "hawaii")
par(mfrow = c(2, 2), mar = c(3.1, 3.1, 1.1, 1.1), mgp = 2:0)
hist(full_el, main = "all data", xlab = "elasticity",
      col = pal[1], breaks = "FD", freq = FALSE)
hist(mle_el, main = "mle", xlab = "elasticity",
      col = pal[2], breaks = 200, xlim = c(-25, 25), freq = FALSE)
hist(naive_el, main = "naive lasso", xlab = "elasticity",
      col = pal[3], freq = FALSE)
hist(dml_el, main = "orth. ML", xlab = "elasticity",
      col = pal[4], freq = FALSE)
```
\normalsize

## Results: full-sample and subsample correlations (code)

\scriptsize
```{r scat_code, eval=FALSE}
ylim <- c(-8, 1)
par(mfrow = c(1, 3), mar = c(3.1, 3.1, 1.1, 1.1), mgp = 2:0)
plot(full_el, mle_el, xlab = "full sample MLE", ylab = "subsample MLE",
      main = "MLE", pch = 21, bg = pal[2], bty = "n", ylim = ylim)
text(x = -6, y = 1, 
      sprintf("R2 = %.02f", summary(lm(full_el ~ mle_el))$r.squared))
plot(full_el, naive_el, xlab = "full sample MLE", ylab = "subsample Naive ML",
      main = "Naive lasso", pch = 21, bg = pal[3], bty = "n", ylim = ylim)
text(x = -6, y = 1, 
      sprintf("R2 = %.02f", summary(lm(full_el ~ naive_el))$r.squared))
plot(full_el, dml_el, xlab = "full sample MLE", ylab = "subsample Orthogonal ML",
      main = "Orthogonal ML", pch = 21, bg = pal[4], bty = "n", ylim = ylim)
text(x = -6, y = 1, 
      sprintf("R2 = %.02f", summary(lm(full_el ~ dml_el))$r.squared))
```
\normalsize

## Results: full-sample and subsample correlations

\scriptsize
```{r scat_plot, echo=FALSE}
ylim <- c(-8, 1)
par(mfrow = c(1, 3), mar = c(3.1, 3.1, 1.1, 1.1), mgp = 2:0)
plot(full_el, mle_el, xlab = "full sample MLE", ylab = "subsample MLE",
      main = "MLE", pch = 21, bg = pal[2], bty = "n", ylim = ylim)
text(x = -6, y = 1, 
      sprintf("R2 = %.02f", summary(lm(full_el ~ mle_el))$r.squared))
plot(full_el, naive_el, xlab = "full sample MLE", ylab = "subsample Naive ML",
      main = "Naive lasso", pch = 21, bg = pal[3], bty = "n", ylim = ylim)
text(x = -6, y = 1, 
      sprintf("R2 = %.02f", summary(lm(full_el ~ naive_el))$r.squared))
plot(full_el, dml_el, xlab = "full sample MLE", ylab = "subsample Orthogonal ML",
      main = "Orthogonal ML", pch = 21, bg = pal[4], bty = "n", ylim = ylim)
text(x = -6, y = 1, 
      sprintf("R2 = %.02f", summary(lm(full_el ~ dml_el))$r.squared))
```
\normalsize

## Specific results
- Besides learning about elasticities as a group, you can look at results for
  individual beers.
- We can see that we shouldn't trust the MLE, and while the naive lasso only
  looks a little worse than the orthogonal ML method, we should probably trust
  the latter.
- What are big sources of heterogeneity?

\scriptsize
```{r het}
B <- coef(dmlhte, s = "lambda.min")[-(1:2), ]
B <- B[B != 0]
head(sort(B))
head(sort(B, decreasing = TRUE))
```
\normalsize

## Other specifics
- What are the most elastic products?

\scriptsize
```{r mill}
names(dml_el) <- upc$DESCRIP
head(sort(dml_el))
head(sort(dml_el, decreasing = TRUE))
```
\normalsize

- It seems like the nonalcoholic beers act differently as a group from the other
  beers.  As a next step you might focus in just on those, to see if you could
  learn more about them in isolation from the others.

