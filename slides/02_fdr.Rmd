---
title: "Multiple testing and Bayesian inference"
author: Tom Parker
date: \today
output:
  beamer_presentation:
    includes:
      in_header: custom.tex
fontsize: 11pt
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(cache=TRUE, scipen = 2, digits = 4)
options(scipen = 2, digits = 4)
```

# Statistical inference and the needle in the haystack

## Old-school tests
- You already know about how to test hypotheses in a regression model.
- Going back to the online spending example, we can check whether individual
  variables seem to be associated with online spending.

\scriptsize
```{r browserlook}
browse <- read.csv("../data/lecture_examples/web-browsers.csv")
summary(lm(spend ~ anychildren + broadband, data = browse))$coef
```
\normalsize

## Old-school testing paradigm
- We have a null hypothesis that, say `broadband` is not significantly related
  to `spend`.
- Under the hypothesis that it really isn't, the estimated $t$ statistic
  $t_{\text{br}} = \hat{\beta}_{\text{br}} /
  \hat{\se}(\hat{\beta}_{\text{br}})$, say, is approximately normally
  distributed.
- We could tell if $t_{\text{br}}$ is not significantly far from zero by
  checking where it falls on the normal distribution.  The p-value is the usual
  way to do this:
  \[ p = \prob{|Z| > |t_{\text{br}}|}, \quad \text{where } Z \sim \calN(0, 1).\]

## What needle?  What haystack?
- Suppose we would like to know who visits our website.  Specifically, which
  other website-visits are most correlated with visiting our website?
- Could we use individual $t$ tests to find the "interesting" websites?
  - We probably shouldn't use an $F$ test (even a partial $F$ test): although
    it is meant for testing many variables, the $F$ or $\chi^2$ reference
    distribution is not good if the number of parameters (websites) is close to
    the number of observations (visitors).
- Then we are trying to find a good way to make many decisions --- this is
  called a *multiple inference* problem.

## Running lots of tests
- Is it ok to use a lot of $t$ tests?
- Consider this example:
  - Suppose that there are 100 covariates that could be related to $y$.
  - Now, suppose that *in the population*, five of these 100 covariates are
    really related to $y$.
  - You run a regression with all the covariates, and collect all the $t$
    statistics.
  - You use them to run 100 tests with size 5\%.
- What is wrong with that?  Well, probably you will find five rejections that
  correspond to the ten really-related variables.  There are 95 variables left,
  and with a 5\% rejection rate, you should find about five of them to be
  really correlated with $y$, since $95 \times 0.05 = 4.75 \approx 5$.  That
  means we will find ten "significant" covariates!
- That is, the **false discovery rate** is close to 1/2.

# Multiple testing and false discovery control

## Multiple testing
- Suppose that you have $K$ hypotheses $H_{k0}$ for $k = 1, \ldots K$.
- One way to be sure that we only get the really significant signals is to
  change the cutoff value for your tests so that the *simultaneous* rejection
  rate of *all* the tests is $\alpha$.
- The probability that any of the tests incurs a type 1 error is called the
  **familywise error rate** (FWER).
  \[FWER = \prob{\text{ reject any true } H_{k0}}.\]

## Aside: the distribution of p-values
- p-values are *statistics* that tell you something about a test.
- If they are small, then they tell you that your test statistic would be
  "weird" under the assumption that the null hypothesis is true.
- What is their distribution (assuming $H_0$ is true)?
- Suppose we just want to reject $H_0$ when our test statistic $T$ is very
  small.  Let $F_T$ be its CDF under $H_0$.
- Then for any test statistic $t$, the p-value is
  \begin{equation*}
    p = F_T(t).
  \end{equation*}

## Aside: p-value distribution calculations
- Now let $F_P$ be the CDF of the (random variable) p-value $P = F_T(T)$.  For a
  constant $\alpha$,
  \begin{equation*}
    \prob{P \leq \alpha} = \prob{F_T(T) \leq \alpha} = \prob{T \leq
    F_T^{-1}(\alpha)} = \alpha,
  \end{equation*}
  using $F_T^{-1}(\cdot)$ as the quantile function of the distribution of $T$.
  - Recall that the quantile function $F_T^{-1}(\alpha) = q$ is the point $q$ in
    the distribution such that the probability $\prob{T \leq q} = \alpha$.
- That is,
  \begin{equation*}
    \prob{P \leq \alpha} = F_P(\alpha) = \alpha \quad \text{ for all } \alpha.
  \end{equation*}
  In other words, the CDF is a *uniform* CDF!

## The Bonferroni correction
- Now, back to familywise error rates.  What if we want to keep $FWER =
  \prob{\text{ reject any true } H_{k0}} \leq \alpha$?
- The **Bonferroni correction** does this by changing the cutoff for each
  individual test.  
- Bonferroni's idea: lower $\alpha$ to $\alpha / K$.  Suppose that
  $\mathcal{K}_0$ is the collection of true hypotheses, and there are $K_0$
  tests in $\mathcal{K}_0$.  Then
  \begin{align*}
    FWER(\text{Bonferroni}) &= \prob{\text{Any type 1 error when rejecting } p_k
    \leq \alpha / K} \\
    {} &= \prob{ \bigcup_{k \in \mathcal{K}_0} \left\{ p_k \leq \alpha / K
    \right\} } \\
    {} &\leq \sum_{k \in \mathcal{K}_0} \prob{p_k \leq \alpha / K} \\
    {} &= K_0 \frac{\alpha}{K} \leq \alpha.
  \end{align*}

## Improving on Bonferroni
- Bonferroni's idea is nice and it works well for a low number of tests (like in
  the tens)
- It turns out that when there are very many tests (like in the thousands), the
  Bonferroni correction makes each test too "strict", usually called
  *conservative*.
- One procedure that is a little less conservative is called *Holm's procedure*,
  and it works like this:

1. Conduct $K$ tests and order their p-values
  \[p_{(1)} \leq p_{(2)} \leq \ldots \leq p_{(K)}.\]
2. Let $k_0$ be the *largest* $k$ such that
   \[ p_{(k)} \leq \frac{\alpha}{K - k + 1}. \]
3. Reject all the hypotheses $H_{k0}$ for $k \leq k_0$.

- It turns out that $FWER(\text{Holm}) \leq \alpha$ and is less conservative
  than Bonferroni.

## False discoveries
- We can be less conservative by controlling something other than the FWER.
- Call individual test rejections "discoveries".
- The **False discovery proportion** (FDP) is the proportion of false
  discoveries that you find when you conduct many tests.
- In the previous example, if we found ten total rejections but five of them
  were false positives, then
  \begin{align*}
    FDP &= \frac{\# \text{ false discoveries}}{\# \text{ total discoveries}} \\
    {} &= \frac{5}{10} = 50\%.
  \end{align*}
- The FDP can be much worse if there are even fewer true signals (and/or more
  tests).

## The false discovery rate
- We can never know what the FDP is for a given data set.
- Maybe we could try to keep its *expected value* controlled.
- The **False discovery rate** (FDR) is the expected value of the false
  discovery proportion from the last slide.
  \begin{equation*}
    FDR = \ex{FDP} = \ex{\frac{\# \text{ false discoveries}}{\# \text{ total
    discoveries}}}.
  \end{equation*}
- What's new?  We need a FDR *target* $q$.  We say that a decision rule controls
  the FDR at level $q \in [0, 1]$ if
  \[ FDR \leq q. \]
- The most popular way to control the FDR uses a procedure called the
  **Benjamini-Hochberg** algorithm.

## Benjamini-Hochberg FDR control
- Looks like Holm's method but it allows in many more discoveries.
- Warning: this procedure controls the exactly FDR under the assumption that the
  tests are *independent*, so for searching through regression $t$ tests it will
  be approximate (but close to exact).
- Here is what you do:

1. Conduct $K$ tests and order their p-values
  \[p_{(1)} \leq p_{(2)} \leq \ldots \leq p_{(K)}.\]
2. Let $k_0$ be the *largest* $k$ such that
   \[ p_{(k)} \leq \frac{k}{K} q. \]
3. Reject all the hypotheses $H_{k0}$ for $k \leq k_0$.

- The BH procedure controls the FDR because $FDR(BH) \leq (K_0 / K) q \leq q$.

## R example
- Let's try these procedures with the browser data.
- I will regress spending on all the other covariates.

\scriptsize
```{r spendingreg}
reg <- lm(spend ~ . - id, data = browse)
regsum <- summary(reg)$coef
regsum
```
\normalsize

## Conventional testing
- What is significant at a 5\% level?

```{r conventional}
alpha <- 0.05
regp <- regsum[-1, 4]
which(regp < alpha)
```

## R example: Bonferroni
- In this regression there are 9 regressors besides the intercept.  So
  Bonferroni says we should be interested in those variables with p-values below
  $\alpha / 9$.  If $\alpha = 0.05$, then $\alpha / 9 \approx `r 0.05 / 9`$.

```{r bonferroni}
K <- length(regp)
bonferroni <- which(regp < alpha / K)
bonferroni
```

- This leaves only consumers who have broadband or live in the western US.

## R example: Holm
- Now let's try the sorting algorithms.

```{r holm}
regp <- sort(regp)
holmvec <- alpha / (K - 1:K + 1)
which(regp <= holmvec)
```

- Because Holm and Bonferroni both control the FWER, they select the same set of
  significant variables here (in general, Bonferroni will usually select fewer).

## R example: Benjamini-Hochberg
- Finally, let's control the FDR instead of the FWER.

```{r BH}
BHq <- 0.1 # 10% FDR
BHvec <- 1:K * BHq / K
which(regp <= BHvec)
```

- This algorithm lets in four variables, unlike the stricter FWER-focused rules.

## Rejection rules: code
- This code produces a picture of the cutoff rules and the (sorted) p-values for
  just the first seven p-values (the last two make the lines difficult to see).

```{r rules_code, eval=FALSE}
Klo <- 7
colvec <- c("black", "red", "green", "blue")
plot(1:Klo, regp[1:Klo], pch = 20)
abline(h = alpha, col = colvec[1])
abline(h = alpha / K, col = colvec[2])
lines(1:Klo, holmvec[1:Klo], col = colvec[3])
lines(1:Klo, BHvec[1:Klo], col = colvec[4])
legend("topleft", c("conventional", "Bonferroni", "Holm", "BH"),
        col = colvec, lty = rep(1, 4), bty = "n")
```

## Rejection rules: the picture
```{r rules, echo=FALSE}
Klo <- 7
colvec <- c("black", "red", "green", "blue")
plot(1:Klo, regp[1:Klo], pch = 20)
abline(h = alpha, col = colvec[1])
abline(h = alpha / K, col = colvec[2])
lines(1:Klo, holmvec[1:Klo], col = colvec[3])
lines(1:Klo, BHvec[1:Klo], col = colvec[4])
legend("topleft", c("conventional", "Bonferroni", "Holm", "BH"),
        col = colvec, lty = rep(1, 4), bty = "n")
```

## One more example: lots and lots of tests
- The FDR concept grew out of work by statisticians with genomic data.  They
  were looking for which genes (out of \alert{lots} of genes) were associated
  with things like diseases.
- The data here is called the *prostate data*.  It is a $6033 \times 102$ matrix
  of genes from 50 patients without cancer and 52 with cancer.
- The $i$th row represents one gene, and the $j$th column is one patient's gene
  "expression" level for gene $i$.  We would like to go through all 6033 genes,
  looking for the ones associated with cancer.
- For each gene we can conduct a usual two-sample $t$ test to see if patients
  with and without cancer have different average gene expression levels.  In the
  data, columns labeled 1 are from patients without cancer and those labeled 2
  are those with cancer.

## Two-sample tests
- For gene $i$, with $\bar{x}_{1i}$ and $\bar{x}_{2i}$ the averages across the
  two groups for those without and with cancer respectively.
- Then for gene $i$, the $t$ statistic is
  \begin{equation*}
    t_i = \frac{\bar{x}_{2i} - \bar{x}_{1i}}{s_i},
  \end{equation*}
  where the (squared) estimated standard error of the numerator is
  \begin{equation*}
    s_i^2 = \frac{ \sum_{j=1}^{50} \left( x_{ij} - \bar{x}_{1i} \right)^2 +
    \sum_{j=51}^{102} \left( x_{ij} - \bar{x}_{2i} \right)^2 }{102 - 2} \times
    \left( \frac{1}{50} + \frac{1}{52} \right)
  \end{equation*}
  (that is, we assume that the variances are equal across the groups).
- Then we will have 6033 $t$ statistics.

## R code
- Here's what it looks like in R.  I made a function that produces a $t$
  statistic for each row of the matrix, then `apply`ed it to the matrix.
- In this case individual statistics should follow a $t_{100}$ distribution.  I
  changed them to $p_i = 2 (1 - F_{t_{100}}(|t_i|))$ to look at p-values.

\scriptsize
```{r twosam}
load("../data/lecture_examples/prostatedata.RData")
tfun <- function(sam) {
  num <- mean(sam[1:50]) - mean(sam[51:102])
  den <- sqrt((49 * var(sam[1:50]) + 51 * var(sam[51:102]))
          / 100 * (1/50 + 1/52))
  num / den
}
tstats <- apply(prostatedata, 1, tfun)
pvals <- 2 * (1 - pt(abs(tstats), df = 100))
```
\normalsize

## Histogram of the p-values
- Now the genes we would like to find are somewhere in the low tail.  But which
  ones are they?

```{r tplot, fig.height=6}
hist(pvals, freq = FALSE, breaks = "FD")
```

## Hunting for genes
- This function produces the "$\alpha$" that you should look for to reject
  tests.

```{r fdr_cut}
fdr_cut <- function(pvals, q){
  pvals <- pvals[!is.na(pvals)]
  N <- length(pvals)
  k <- rank(pvals, ties.method = "min")
  alpha <- max(pvals[ pvals <= (q * k / N) ])
  return(alpha)
}
```

- For example, with a FDR of 10\%, i.e., $q = 0.1$, we get the cutoff
  `r fdr_cut(pvals, 0.1)` (I wrote `fdr_cut(pvals, 0.1)`).
- How many genes?
```{r count}
cutoff <- fdr_cut(pvals, 0.1)
sum(pvals <= cutoff)
```

# Bayesian inference

## Bayes' rule reminder
- Sometimes (in the ISLR book, for example) **Bayes' rule** is used to conduct
  inference.  What's that all about?
- Recall that Bayes' rule is a way of *updating* a distribution given some new
  information.
- The classic rule is
  \begin{equation*}
    \prob{B | A} = \frac{\prob{A | B} \prob{B}}{\prob{A}}.
  \end{equation*}
- This is really just a rewrite of conditional probability rules, but it has
  some power when we make $B$ and $A$ our parameter and our data.

## Bayesian ingredients
- Suppose that we would like to know about a parameter $\theta$, and we see
  observations $X$.
- Bayesians put a *distribution* on their parameters(!).  This distribution is
  called a **prior distribution**.  It's supposed to reflect what you already
  know from experience.  I'll call it $g(\theta)$.
- You also need the **likelihood** of a parameter given data.  Recall that the
  likelihood is just the density of $X$ for a parameter $\theta$, but it is
  viewed as a function of $\theta$ for a given $x$.  This one I will call $f(x,
  \theta)$.
- Given a likelihood and a prior distribution, we can make a **posterior**
  distribution for $\theta$ given $X$.  Call this $g(\theta | x)$.  

## Calculating a posterior distribution
- Bayes' rule says
  \begin{equation*}
    g(\theta | x) = \frac{f(x, \theta) g(\theta)}{f(x)} \propto f(x, \theta)
    g(\theta).
  \end{equation*}
- The denominator is the **marginal** density of $X$.  You could estimate this,
  for example, or use the relationship with the likelihood
  \begin{equation*}
    f(x) = \int f(x, \theta) g(\theta) \ud \theta.
  \end{equation*}
  This is often a difficult part, and wherever possible, we ignore it.
  - It isn't usually important, because it is just a constant that is used to
    make the right-hand side sum to 1.
- This is the motivation for looking at posterior **odds ratios** as well: we
  can say whether $\theta_1$ or $\theta_2$ is more likely by examining
  \begin{equation*}
    \frac{g(\theta_1 | x)}{g(\theta_2 | x)}  = \frac{f(x, \theta_1)
    g(\theta_1)}{f(x, \theta_2) g(\theta_2)}.
  \end{equation*}

## Twins example
- Here is an example.  Suppose that a woman gets a sonogram and finds that
  she will be having twin boys.  She asks the doctor, "what is the probability
  that the twins will be identical or fraternal?".  The doctor says that one
  third of twin births are identical, and two thirds are fraternal.
- In this example, we know that the gender of the twins is the same.  They could
  be fraternal or identical.
- The doctor has given the woman a prior density for the fraternal/identical
  event.

## Information about twins
- Remember also that identical twins always have the same sex.
- Then this is what we know (probabilistically) about twins:
  \vspace{1em}
  \begin{center}
    \begin{tabular}{r|c|c|l}
      {} & Same sex & Different & {} \\
      \hline
      Identical & 1/3 & 0 & 1/3 \\
      \hline
      Fraternal & 1/3 & 1/3 & 2/3 \\
      \hline
    \end{tabular}
  \end{center}
  \vspace{1em}
- We're able to fill in the chart because:
  - the doctor gives us the marginal $1/3$ and $2/3$
  - identical twins have to have the same sex
  - we assume that the probability for any pair of genders with fraternal twins is the same

## Working with twins probabilities
- Using Bayes' rule, we can write
  \begin{align*}
    \prob{\text{identical} | \text{same}} &= \frac{\prob{\text{same} |
    \text{identical}} \prob{\text{identical}}}{\prob{\text{same}}}
    {} &= \frac{1 \times 1/3}{2/3}.
  \end{align*}
- More interesting to calculate the *odds* of the twins being identical or
  fraternal:
  \begin{align*}
    \frac{\prob{\text{identical} | \text{same}}}{\prob{\text{fraternal} |
    \text{same}}} &= \frac{\prob{\text{same} | \text{identical}}
    \prob{\text{identical}} }{\prob{\text{same} | \text{fraternal} }
    \prob{\text{fraternal}} } \\
    {} &= \frac{1}{1/2} \times \frac{1/3}{2/3} = 1.
  \end{align*}
- The odds are even that the twins are identical or fraternal.  The doctor's
  prior odds are balanced by the likelihood ratio (which are like odds from the
  data).

## Other uses for the posterior distribution
- What do you do with the posterior distribution more generally?
- Odds ratios are good for examining just a few possibilities, but if there are
  many possible $\theta$, then we need a rule for choosing one.
- When trying to estimate $\theta$, it's kind of obvious: people use the
  posterior mean, median or mode.

## Beta/binomial example
- Imagine that we flip a coin one time, and $p = \prob{\text{heads}}$ is
  unknown.
- Flipping a coin is an example of a **Bernoulli** trial, which has density
  (or likelihood)
  \begin{equation*}
    f(x, p) = p^x (1 - p)^{1 - x}.
  \end{equation*}
- Suppose that we use a **beta** distribution for our prior.  The beta
  distribution is a "trick" distribution called a **conjugate prior** because it
  turns out that the posterior is also a beta distribution.
- The $\text{Beta}(\alpha, \beta)$ density is
  \begin{equation*}
    g(p) \propto p^{\alpha - 1} (1 - p)^{\beta - 1}, \quad p \in [0, 1],
  \end{equation*}
  where I ignored the constant that makes this integrate (over $p$) to 1.  For
  example, $\text{Beta}(1, 1)$ is a uniform distribution.  Try plotting
  `dbeta()` densities in R for different values of the parameters.

## Beta/binomial
- It turns out that if $g(p) = \text{Beta}(\alpha, \beta)$, after you see $x =
  1$ or $x = 0$ from a Bernoulli trial, we can find that
  \begin{equation*}
    g(p | x) = \text{Beta}(\alpha + x, \beta + (1 - x)).
  \end{equation*}
- That means that if we do $n$ Bernoulli trials, then
  \begin{equation*}
    g(p | x_1, \ldots, x_n) = \text{Beta} \left( \alpha + \sum_i x_i, \beta + n
    - \sum_i x_i \right).
  \end{equation*}
- Then we could use the beta mean formula
  \begin{equation*}
    p \sim \text{Beta}(\alpha, \beta) \quad \Rightarrow \quad \ex{p} =
    \frac{\alpha}{\alpha + \beta}
  \end{equation*}
  to estimate $p$ given our sample.

## Beta/binomial code example
- This simulates $n$ coin flips and calculates a posterior mean.  Play with the
  true $p$ for example, and/or the prior $\alpha$ and $\beta$, and/or $n$.

\scriptsize
```{r bin_code}
set.seed(8675309)
p_true <- 0.75
a0 <- 1
b0 <- 1
n <- 50
sam <- rbinom(n, size = 1, prob = p_true)
a_post <- a0 + sum(sam)
b_post <- b0 + n - sum(sam)
p_post <- a_post / (a_post + b_post)
p_post
```
\normalsize

## Picture of prior/posterior densities

\scriptsize
```{r bin_plot, fig.height=5.5}
curve(dbeta(x, a0, b0), from = 0, to = 1, col = "green", ylim = c(0, 10))
curve(dbeta(x, a_post, b_post), add = TRUE, col = "blue")
abline(v = p_post, col = "red")
abline(v = mean(sam), col = "purple", lty = 2)
abline(v = p_true, lty = 3)
legend("topleft", c("prior dens", "post. dens", "post. mean", 
        "mle", "true mean"), lty = c(1, 1, 1, 2, 3), 
        col = c("green", "blue", "red", "purple", "black"), bty = "n")
```
\normalsize

## Hunting for non-null hypotheses again
- Let's go back to conducting many tests.
- Suppose that the genes are either "null" or "non-null".
- Define
  \begin{equation*}
    \pi_0 = \prob{\text{gene is null}}, \quad \pi_1 = \prob{\text{gene is
    non-null}}
  \end{equation*}
  and
  \begin{gather*}
    f_0, F_0 \quad \text{density and CDF for null gene p-values}, \\
    f_1, F_1 \quad \text{density and CDF for non-null gene p-values}
  \end{gather*}
- If gene $k$ is a null gene, then the p-value for test $k$, $p_k$, should be
  distributed uniformly.
- The density and distribution functions of all the genes are a *mixture*
  distribution with 
  \begin{equation*}
    f(p) = \pi_0 f_0(p) + \pi_1 f_1(p), \quad F(p) = \pi_0 F_0(p) + \pi_1
    F_1(p).
  \end{equation*}

## Cutoffs and FDRs
- Suppose we choose a cutoff value $p^*$ such that for any $p_k \leq p^*$, we
  reject the $k$th null hypothesis.
- The FDR of such a procedure is (let $P$ be the p-value of the $k$th test)
  \begin{align*}
    FDR(p^*) &= \prob{\text{ gene is null } | P \leq p^*} \\
    {} &= \frac{\prob{P \leq p^* | \text{ gene is null }} \prob{\text{ gene is
    null }}}{\prob{P \leq p^*}},
  \end{align*}
  where the second equality is from Bayes' rule.
- At first, this doesn't look very helpful: if we use the notation from last
  slide, we find
  \begin{equation*}
    FDR(p^*) = \frac{F_0(p^*) \pi_0}{\pi_0 F_0(p^*) + \pi_1 F_1(p^*)}.
  \end{equation*}
  But $\pi_0$, $\pi_1$ and $F_1$ are unknown!

## Estimating the prior
- We could ignore the formula in the denominator of the last expression, and
  remember that it is equal to $F(p)$.
- Estimate $F(p)$ with
  \begin{equation*}
    \hat{F}(p) = \frac{\# \{p_k \leq p\}}{K}
  \end{equation*}
- Then if we choose $k_0$ such that $p_{(k_0)}$ is the largest p-value with
  $p_{(k_0)} \leq \frac{k_0}{K} q$, because $\hat{F}(p_{(k_0)}) = k_0 / K$,
  \begin{align*}
    \widehat{FDR}(p_{(k_0)}) &= \frac{F_0(p_{(k_0)}) \pi_0}{\hat{F}(p_{(k_0)})}
    \\
    {} &= \frac{p_{(k_0)} \pi_0}{k_0 / K} \\
    {} &\leq \frac{(k_0 / K) q \pi_0}{k_0 / K} \\
    {} &= \pi_0 q \leq q,
  \end{align*}
  since $\pi_0 \leq 1$ (it's probably pretty close to 1).

## Benjamini-Hochberg and empirical Bayes
- A procedure in which we estimate the prior(!) is called an **empirical Bayes**
  procedure.  It's not really Bayesian, but it's not really frequentist either.
- The BH algorithm suggests that we reject tests for which
  \begin{equation*}
    FDR(p) = \frac{\pi_0 F_0(p)}{F(p)} < q.
  \end{equation*}
- We don't really see this when describing the method, which has been turned
  around to get an easy rule using the p-values.
- We can call $\widehat{FDR}(p)$ the posterior probability that a null
  hypothesis is true given we have observed that its p-value is very small,
  where we have used an empirical Bayes procedure to find $\widehat{FDR}(p)$.
  - The "local FDR" $\pi_0 f_0(p) / f(p)$ tells us the posterior odds that a
    hypothesis is null.

