---
title: Classification
author: Tom Parker
date: \today
output:
  beamer_presentation:
    slide_level: 2
    includes:
      in_header: custom.tex
fontsize: 11pt
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(cache=TRUE, scipen = 2, digits = 4)
options(scipen = 2, digits = 4)
```

## The general idea
- Sometimes, the goal of our analysis is not to predict the value of a
  quantitative response variable.
- **Classification** is a prominent example: suppose we have $m$ categories of
  response.  If I see a new set of covariates $x_{new}$, what is the probable
  category for $y_{new}$?  Your predictions are classifications.
- The response need not be ordered in any way.
- First we will look at the nuances involved in classifying objects into two
  categories (like pass/fail type things).
- Next we will explore multi-class classification with two tools: K-nearest
  neighbour classifiers and multinomial logistic regression models.

# Two-class classification: some special terms

## Costs: loss and risk
- It is common in statistics to construct a **loss function** that tells you
  what kind of cost you pay for making a decision.
  - The squared distance between a prediction and an observation is used in
    least squares estimation: $L(\hat{y}) = (y - \hat{y})^2$.
- Because we only have random samples, we try to minimize the *expected* loss,
  which is called the **risk** of our decision:
  \begin{equation*}
    R(\beta) = \ex{(Y_i - X_i\tr \beta)^2},
  \end{equation*}
  for example.
- We estimate this with the in-sample analog, the average.
- Note that this notion of risk is different than the one used in finance where
  they often equate the words "risk" and "variance".

## Costs for classification
- We'll see below that squared error loss isn't really appropriate for
  classification.
- For now, suppose that $y \in \{0, 1\}$ and we would like to predict it.
- How we can make mistakes?  We could have
  1. False positives: we predict $\hat{y} = 1$ when $y = 0$.
  1. False negatives: we predict $\hat{y} = 0$ when $y = 1$.
- Generally speaking, you don't need to weigh these costs the same.
- For example, making a loan can have asymmetric costs.  Suppose you loan \$100
  to someone and they promise to pay you \$125 back.  If the probability of
  default is 10%, then the expected value of your loan, which is $(-1)$ times
  the risk of your loan, is
  \begin{align*}
    R(\text{loan}) &= \prob{\text{loss 1}} \text{loss 1} + \prob{\text{loss
    2}} \text{loss 2} \\
    {} &= \quad 0.1 \;\; \times \;\; 100 \;\; + \quad 0.9 \;\; \times \;\; (-25).
  \end{align*}

## Logistic regression
- If we know the probabilities of default/non-default, then we can accurately
  measure the risk of a loan.
- For better or for worse, this is a very common application of classification
  in business.
- In the context of extending credit, banks will be interested in knowing how
  well they predict bad risks, and what kind of cutoff they want to set to know
  when to lend or not.
- You have to make a cutoff for when to look at a predicted probability of
  success and declare the prediction to really indicate success.  Such a cutoff
  is called a **classification rule**.

## South German credit data
- The next example uses the *South German Credit* data, which is quite popular
  for classification exercises (see
  [this
  link](http://archive.ics.uci.edu/ml/datasets/South+German+Credit+%28UPDATE%29)
  to the UCI machine learning repository or the class repository, where it is
  also stored).
- There are 1000 observations of bank customers who took out loans from a bank
  in West Germany in the 1970s, and whether they paid their loans back.  The
  "bad" customers are oversampled to get enough observations about them to
  distinguish them from "good" customers.  There are 700 "good" and 300 "bad"
  customers.
- The response variable is `credit_risk`, which tells us whether or not a
  customer turned out to be a "good risk" or a "bad risk": whether they caused
  the bank problems by not paying their loan back.
- These observations are a little problematic because they were collected from
  a bank that had already given customers loans.  So they aren't really good for
  saying whether just anybody should get a loan.

## Reading data, estimating a model
- We've seen all this before: going to regress `credit_risk` on everything,
  interacted.
- I previously `source()`ed the file `read_SouthGermanCredit.R` and saved the
  object `dat` as `german_credit.RData`, which you can use if you're using R.

\scriptsize
```{r credit_read}
load("../data/SouthGermanCredit/german_credit.RData") # data frame called dat
# Can get this file yourself by source()-ing read_SouthGermanCredit.R.
library(glmnet)
# I put this in memory because the data set is not too big.
# Otherwise use glmnetUtils to build it on the fly in glmnet().
credX <- makeX(model.frame(credit_risk ~ . ^2, data = dat)[, -1])
badgood <- dat$credit_risk
cred_est <- glmnet(credX, badgood, family = "binomial")
cred_cv <- cv.glmnet(credX, badgood, family = "binomial")
```
\normalsize

## Look at what we've got
- Here are the usual plots.

\scriptsize
```{r credit_lassoplots, fig.height=5}
library(colorspace)
pal_lasso <- sequential_hcl(10, "oranges")
par(mfrow = c(1, 2), mar = c(3.1, 3.1, 2.1, 1.1), mgp = 2:0)
plot(cred_cv)
plot(cred_est, xvar = "lambda", col = pal_lasso)
abline(v = log(cred_cv$lambda.min), lty = 3)
```
\normalsize

## A few more checks
- We can look at some more features of the chosen model.  I used the one that
  minimized the 10-fold CV criterion from `cv.glmnet()`.

\scriptsize
```{r credit_morelooks}
cred_est$dev.ratio[cred_cv$index[1]]
cred_cv$nzero[cred_cv$index[1]]
```
\normalsize

## A few more checks
- How do predicted probabilities of good credit look plotted against the true
  classes?

\scriptsize
```{r credit_box, fig.height=6}
pal_box <- diverging_hcl(2, "blue-red")
pred <- predict(cred_cv, newx = credX, s = "lambda.min", type = "response")
boxplot(pred ~ badgood, xlab = "bad/good risk",
        ylab = "prob(good)", col = pal_box)
```
\normalsize

## Misclassification rates
- Now, we are mostly concerned (especially if we work for the bank) with knowing
  how well we predict good or bad credit risks.
- There are many popular ways to measure that, so many that it can be very
  confusing.
- Two rates that are like our FDR analysis from before:
  \begin{equation*}
    \text{False \alert{Discovery} Rate (FDR)} = \frac{\text{expected } \# \text{
    false positives}}{\# \text{ classified positive}}
  \end{equation*}
  and
  \begin{equation*}
    \text{False \alert{Omission} Rate (FOR)} = \frac{\text{expected } \# \text{
    false negatives}}{\# \text{ classified negative}}.
  \end{equation*}

## Roll your own loss function
- Recall the simple loan example from before.  Let's abstract from the
  probabilities there, and suppose that $p$ is the probability that a customer
  is a good risk.
- At what $p$ would you break even?  Write $B$ for the loss from a bad customer,
  and $G$ for the loss from a good customer:
  \begin{align*}
    (1 - p) B + p G &\stackrel{set}{=} 0 \\
    B + p (G - B) &= 0 \\
    p &= \frac{B}{B - G}.
  \end{align*}
- So if $B = 100$ and $G = -25$, then the break-even probability of good
  customers is
  \begin{equation*}
    p = \frac{100}{100 - (-25)} = 0.8.
  \end{equation*}
- What if we used a 0.8 cutoff to screen customers?  If $\prob{\text{good}} \geq
  0.8$, we will give them a loan.

## FDR and FOR for the German credit data
- For this model the response variable has `bad` as the first category, so if
  you don't change anything, then the predicted probabilities are the
  probability that a borrower is `good`.
  - You could change this in R by re-leveling the variable: 

\scriptsize
```{r ex, eval=FALSE} 
badgood <- factor(dat$credit_risk, levels = c("good", "bad"))
```
\normalsize

- If we gave loans out when $\hat{\text{P}}\{\text{good}\} \geq 0.8$, then what
  are the FDR and FOR?

\scriptsize
```{r fpr}
rule <- 4/5
FDR <- sum( (pred > rule)[badgood == "bad"] ) / sum(pred > rule)
FOR <- sum( (pred < rule)[badgood == "good"] ) / sum(pred < rule)
FDR
FOR
```
\normalsize

## Another target: sensitivity and specificity
- A classification rule is called **sensitive** if it predicts successes for
  most of the truly successful observations.
- A rule is called **specific** if it predicts non-success for most of the
  non-successful observations.
- For our credit example, this means that we should find the proportion of those
  labeled `good` out of all the `good` observations and same with `bad`
  (sens./spec. respectively).

\scriptsize
```{r sensspec}
rule <- 4/5
sensitivity <- sum( (pred > rule)[badgood == "good"] ) / sum(badgood == "good")
specificity <- sum( (pred < rule)[badgood == "bad"] ) / sum(badgood == "bad")
sensitivity
specificity
```
\normalsize

## Misclassification and Type I/II errors
- Sensitivity and specificity are tied to type I and type II errors.
- Recall that a **type I error** is when a condition should be null and you
  declare it to be non-null.  In the context of (binary) prediction, this would
  be like declaring something a 1 when it is a 0.
- On the other hand, **type II error** is the opposite: declaring something null
  when it is non-null.  Like calling something a 0 when it is truly a 1.
- Mirroring classical statistics, we would like to keep both of these low, or to
  keep the probability of type I error low while maximizing 1 minus the
  probability of type II error (like the power of a test).
- Given the other definitions,
  1. Sensitivity = power = $\frac{\text{expected } \# \text{ true positives
     }}{\# \text{ truly positive }}$ = 1 - Type II error prob.
  1. Specificity = $\frac{\text{expected } \# \text{ true negatives }}{\# \text{
     truly negative }}$ = 1 - Type I error prob.

## The confusion matrix
- A **confusion matrix** is often used to show how a classifier did.
- Here $\hat{P}$ is the number of predicted positives and $\hat{N}$ is the
  number of predicted negatives.
- Note that "true negatives" or "true positives" refers to when the test
  correctly predicts the condition (not just for when the condition is P/N).
- This results in two *new* names, the **True Positive Rate** and **True
  Negative Rate**, which are other words for Sensitivity and Specificity
  respectively.

\begin{center}
\begin{tabular}{r|c|c|l}
True/Pred. & null & non-null & total \\
\hline
null & True negatives & False positives & $N$ \\
\hline
non-null & False negatives & True positives & $P$ \\
\hline
total & $\hat{N}$ & $\hat{P}$ & {}
\end{tabular}
\end{center}

1. $FDR = FP / \hat{P}$
1. $FOR = FN / \hat{N}$
1. $TPR = \text{ sensitivity } = TP / P$
1. $TNR = \text{ specificity } = TN / N = 1 - FP / N = \text{ \alert{false positive
   rate (FPR)}}$

## ROC curve
- Now, having seen a few rules and hopefully having played with the `rule`
  argument in the code, you see that we have different FPR/FNR and
  sensitivity/specificity for any cutoff that we choose.
- A convenient way to summarize all the choices is using a **ROC** curve (for
  *receiver operating characteristic* (a name coming from electrical
  engineering, where it was first developed for analyzing signal detection).
- It is most typically a plot of the *true* positive rate on the vertical axis
  against the false positive rate (FPR) on the horizontal axis.  Watch out, the
  actual plot definition could be different.
  - The true positive rate is the number predicted positive out of the total
    positive.

## ROC on a split sample
- We'd like to know what the OOS ROC curve is.
- Here is one way to estimate the ROC curve using a sample: split the sample in
  two, and predict one half using the other.

\scriptsize
```{r roc_split}
set.seed(321)
test <- sample.int(1000, 500)
credhalf <- cv.glmnet(credX[-test, ], badgood[-test], family = "binomial")
pred_oos <- predict(credhalf, credX[test,], s = "lambda.min", type = "response")
badgood_oos <- badgood[test]
```
\normalsize

## ROC curve: setup
- The following code gets the vectors of true and false positives ready for us
  to plot.

\scriptsize
```{r roc_data}
n <- length(badgood)
thresholds <- matrix(rep(seq(0, 1, length = 101), n),
                      ncol = 101, byrow = TRUE)
Q <- as.vector(pred) > thresholds
fpr.vec <- colMeans(Q[badgood == "bad", ])
tpr.vec <- colMeans(Q[badgood == "good", ])
```
\normalsize

## ROC curve: code
- Here's how I plotted it.

\scriptsize
```{r roc_code, eval=FALSE}
par(mar = c(3.1, 3.1, 1.1, 1.1), mgp = 2:0)
plot(fpr.vec, tpr.vec, type = "l", xlab = "False positive rate",
      ylab = "True positive rate", main = "ROC curve")
abline(a = 0, b = 1, lty = 2)
rule_bayes <- 1/2
rule_strict <- 4/5
points(x = mean((pred > rule_strict)[badgood == "bad"]),
        y = mean((pred > rule_strict)[badgood == "good"]),
        cex = 1.5, pch = 20, col = pal_box[1])
points(x = mean((pred > rule_bayes)[badgood == "bad"]),
        y = mean((pred > rule_bayes)[badgood == "good"]),
        cex = 1.5, pch = 20, col = pal_box[2])
legend("bottomright", legend = c("strict", "50/50"),
        pch = 20, pt.cex = 2, col = pal_box, bty = "n")
```
\normalsize

## ROC curve: code
- Being close to the upper left-hand corner is best: lots of
  true positives, few false positives.

\scriptsize
```{r roc_plot, echo=FALSE, fig.height=3, fig.width=3, fig.align='center'}
par(mar = c(3.1, 3.1, 1.1, 1.1), mgp = 2:0)
plot(fpr.vec, tpr.vec, type = "l", xlab = "False positive rate",
      ylab = "True positive rate", main = "ROC curve")
abline(a = 0, b = 1, lty = 2)
rule_bayes <- 1/2
rule_strict <- 4/5
points(x = mean((pred > rule_strict)[badgood == "bad"]),
        y = mean((pred > rule_strict)[badgood == "good"]),
        cex = 1.5, pch = 20, col = pal_box[1])
points(x = mean((pred > rule_bayes)[badgood == "bad"]),
        y = mean((pred > rule_bayes)[badgood == "good"]),
        cex = 1.5, pch = 20, col = pal_box[2])
legend("bottomright", legend = c("strict", "50/50"),
        pch = 20, pt.cex = 2, col = pal_box, bty = "n")
```
\normalsize

# K-nearest neighbours

## The general idea
- The K-nearest-neighbour idea is a simple one.
- Given new covariates $x_{new}$, we guess a class to $y_{new}$ just by looking
  at what $x_i$ are *closest* (i.e., in Euclidean distance) to $x_{new}$.
- Here is a classic statistics data set: the Anderson/Fisher Iris data
  available in base R (try `?iris`).

\scriptsize
```{r iris_read}
data(iris)
summary(iris)
```
\normalsize

## An iris
- This is *Iris versicolor*, or *northern blue flag*.  Sepals are the big
  "petals" and petals are the little (rounded) ones in the middle.
![iris_versicolor_from_wikipedia](./Blue_Flag,_Ottawa.jpg)

## R example: setup
- I just picked Petal Length and Sepal Length to see if I could classify a new
  flower given a random sample of ten others (so we can see what's going on).

\scriptsize
```{r iris_setup}
set.seed(42)
dat <- iris[, c("Sepal.Length", "Petal.Length", "Species")]
n <- nrow(iris)
sam_index <- sample.int(n, size = 10)
new_index <- sample.int(n, size = 1)
sam_features <- dat[sam_index, 1:2]
sam_response <- dat[sam_index, 3]
f_new <- dat[new_index, 1:2]
true_species <- dat[new_index, 3] # just for checking later
true_species
```
\normalsize

## R example: distances (code)
- We could do this manually:

\scriptsize
```{r iris_dist_code, eval=FALSE}
library(colorspace)
pal <- sequential_hcl(3, "red-blue")
dvec <- dist(rbind(f_new, sam_features))[1:10]
dsort <- sort(dvec, index.return = TRUE) # get indexes of closest
K <- 4
plot(sam_features$Sepal.Length, sam_features$Petal.Length,
      pch = 20, cex = 2, col = pal[sam_response], xlab = "Sepal length",
      ylab = "Petal length")
points(f_new, pch = 13, cex = 2)

for (i in 1:K) {
  segments(as.numeric(f_new[1]), as.numeric(f_new[2]),
            sam_features[dsort$ix[i], 1], sam_features[dsort$ix[i], 2], 
            lty = 3)
}
legend("bottomright", levels(dat$Species), pch = 20,
        pt.cex = 2, col = pal, bty = "n")
```
\normalsize

## R example: distances (picture)

```{r iris_dist_plot, echo=FALSE}
library(colorspace)
pal <- sequential_hcl(3, "red-blue")
dvec <- dist(rbind(f_new, sam_features))[1:10] # first column is distances from inew
dsort <- sort(dvec, index.return = TRUE) # get indexes of closest
K <- 4
plot(sam_features$Sepal.Length, sam_features$Petal.Length,
      pch = 20, cex = 2, col = pal[sam_response], xlab = "Sepal length",
      ylab = "Petal length")
points(f_new, pch = 13, cex = 2)

for (i in 1:K) {
  segments(as.numeric(f_new[1]), as.numeric(f_new[2]),
            sam_features[dsort$ix[i], 1], sam_features[dsort$ix[i], 2], lty = 3)
}
legend("bottomright", levels(dat$Species), pch = 20,
        pt.cex = 2, col = pal, bty = "n")
```

## R example: `knn()`
- The R package `class` supplies the function `knn`, which is much faster and
  more reliable than calculating distances by hand.

```{r knn_iris}
library(class)
nn_guess <- knn(sam_features, test = f_new,
                cl = sam_response, k = K)
nn_guess
true_species
```

- In this example, K was four.

## The picture (code)

\scriptsize
```{r iris_color_code, eval=FALSE}
plot(sam_features$Sepal.Length, sam_features$Petal.Length,
      pch = 20, cex = 2, col = pal[sam_response], xlab = "Sepal length",
      ylab = "Petal length")
points(f_new, pch = 13, cex = 2)
for (i in 1:K) {
  segments(as.numeric(f_new[1]), as.numeric(f_new[2]),
            sam_features[dsort$ix[i], 1], sam_features[dsort$ix[i], 2], 
            lty = 3)
}
points(as.numeric(f_new[1]), as.numeric(f_new[2]), pch = 20,
        cex = 2, col = pal[match(nn_guess, levels(sam_response))])
legend("bottomright", levels(dat$Species), pch = 20,
        pt.cex = 2, col = pal, bty = "n")
```
\normalsize

## The picture

```{r iris_color_plot, echo=FALSE}
plot(sam_features$Sepal.Length, sam_features$Petal.Length,
      pch = 20, cex = 2, col = pal[sam_response], xlab = "Sepal length",
      ylab = "Petal length")
points(f_new, pch = 13, cex = 2)
for (i in 1:K) {
  segments(as.numeric(f_new[1]), as.numeric(f_new[2]),
            sam_features[dsort$ix[i], 1], sam_features[dsort$ix[i], 2], 
            lty = 3)
}
points(as.numeric(f_new[1]), as.numeric(f_new[2]), pch = 20,
        cex = 2, col = pal[match(nn_guess, levels(sam_response))])
legend("bottomright", levels(dat$Species), pch = 20,
        pt.cex = 2, col = pal, bty = "n")
```

## A more complicated example: glass
- This sample consists of 214 shards of glass, with measurements about the
  *refractive index* (RI) and weight of elements `Na`, `Mg`, `Al`, `Si`, `K`,
  `Ca`, `Ba` and `Fe` in each shard.
- There are six types of glass:
  1. `WinF`: window float glass (refers to how it was produced)
  2. `WinNF`: window non-float glass
  3. `Veh`: vehicle window
  4. `Con`: container
  5. `Tabl`: tableware
  6. `Head`: vehicle headlamp

- The data are available in the `MASS` package that comes with base R.
\scriptsize
```{r glass_read}
library(MASS) ## a library of example datasets
data(fgl) ## loads the data into R; see help(fgl)
```
\normalsize

## Plot the data (code)
- Plots of all eight elements

\scriptsize
```{r glassplot_code, eval=FALSE}
library(colorspace)
pal <- sequential_hcl(6, "viridis")
par(mfrow = c(2, 4), mar = c(3.1, 3.1, 1.1, 1.1), mgp = 2:0)
for (j in 2:9) {
  plot(fgl[, j] ~ type, data = fgl, col = pal, ylab = names(fgl)[j])
}
```
\normalsize

## Plot the data (picture)

```{r glassplot_plot, echo=FALSE}
library(colorspace)
pal <- sequential_hcl(6, "viridis")
par(mfrow = c(2, 4), mar = c(3.1, 3.1, 1.1, 1.1), mgp = 2:0)
for (j in 2:9) {
  plot(fgl[, j] ~ type, data = fgl, col = pal, ylab = names(fgl)[j])
}
```

## Features of the covariates
- In the previous slides, you can see that some covariates are more helpful than
  others.
- For example, `Ba` is a very powerful **discriminator** of headlamp glass,
  because the other glasses have only trace amounts of it.
- Others, like `Si`, don't look so helpful.
- However don't forget that these are *partial* plots: we don't see multivariate
  combinations that might point at one glass or another.

## Glass using `knn()`
- To use `knn()` we have to change the observations to be `numeric`.
- It's helpful to `scale()` the observations so that distances are in terms of
  standard deviations.
- Note that the tenth column can stay a `factor`, since it is the response.

\scriptsize
```{r glass_prepare}
x <- scale(fgl[, 1:9])
apply(x, 2, sd) # Look what happened!
y <- fgl$type
```
\normalsize

## Try a few Ks.
- Here are `k = 1` and `k = 5` for example:
- You have to give `knn()` training data, and the subset of ten randomly-chosen
  observations is used to imagine that we have new pieces of glass.

\scriptsize
```{r k_15_code}
test <- sample.int(214, 10)
nearest1 <- knn(train = x[-test, ], test = x[test, ], cl = y[-test], k = 1)
nearest5 <- knn(train = x[-test, ], test = x[test, ], cl = y[-test], k = 5)
answers <- data.frame(y[test], nearest1, nearest5)
```
\normalsize

## Results for `k = 1` and `k = 5`

\scriptsize
```{r k_15}
answers
```
\normalsize

## Try all the Ks?
- Here's a loop through `k = 1` to `k = 10`.

\scriptsize
```{r allk_code}
kmax <- 10
for (near in 1:kmax) {
assign(paste("near", near, sep = ""), knn(train = x[-test, ], 
        test = x[test, ], cl = y[-test], k = near))
}
all_ans <- data.frame(y[test], mget(paste("near", 1:kmax, sep = "")))
```
\normalsize

## Results for all `k`

\scriptsize
```{r allk_res}
all_ans
```
\normalsize

## Drawbacks of KNN estimation
- There isn't really a model with KNN estimates.  We just look for closest
  points.  This may or may not be a drawback.
- Also, the solution is **unstable**.
  - In the sense that the "probability" of my example flower being `versicolor`
    is 1 when $K=1$, 1/2 when $K=2$, 2/3 when $K=3$\ldots
- In other words, as we change K, the predicted class changes abruptly.
- That makes it difficult to decide on a good value for K.
- Finally, since you need to compute $n$ distances for each new prediction, this
  method can be computationally costly.

# Multinomial logistic regression

## Measuring mistakes in prediction
- The squared error loss for predictions $\ex{(y_{new} - \hat{y}_{new})^2}$ that
  has been used so far doesn't make sense for classifying $K > 2$ categories!
- One direct solution is to estimate the chance of misclassification.
- Let $g(x)$ denote a function that maps covariates to classes.
- Logistic regression arises from the way that we treat errors in prediction
  using $\hat{g}$.

## Loss and risk of classifying
- Usually the loss for classification with $K$ classes is just an indicator of
  whether the prediction is wrong:
  \begin{equation*}
    \text{error}_i = I(y_{new} \neq \hat{g}(x_{new})).
  \end{equation*}
- This loss doesn't specify asymmetric costs for false positives/negatives.
- The in-sample risk is the average of the conditional expectation of the $i$
  errors (conditional on $x_1, \ldots, x_n$):
  \begin{align*}
    R_n(\hat{g}) &= \frac{1}{n} \sum_{i=1}^n \ex{I(y_i \neq \hat{g}(x_i)) | x_i}
    \\
    {} &= \frac{1}{n} \sum_{i=1}^n \prob{y_i \neq \hat{g}(x_i) | x_i}.
  \end{align*}

## Making a model
- One way to proceed is to make a model that depends on the classes.
- Suppose that there are $K$ classes, indexed by $k = 1, \ldots, K$.
- Then we could say each type dictates a distribution of $X$:
  \begin{gather*}
    X | Y = k \sim F_k(\cdot), \quad \text{with density} \quad f_k(\cdot), \\
    \prob{Y = k} = \pi_k.
  \end{gather*}
- Furthermore (to make it easier to model the way $Y$ depends on $X$) it may be
  that each density function has parameters, like
  \begin{equation*}
    f_k(x) = f(x | \theta_k).
  \end{equation*}

## Minimizing risk
- You would like to minimize the risk (expected loss) of misclassification.
- Supposing that parameters were known, the risk
  \begin{equation*}
    \prob{Y_{new} \neq \hat{g}(X_{new}) | X_{new} = x_{new}} 
    = 1 - \prob{Y_{new} = \hat{g}(X_{new}) | X_{new} = x_{new}},
  \end{equation*}
  so you can minimize the risk by maximizing
  \begin{equation*}
    \prob{Y_{new} = k | X_{new} = x_{new}}.
  \end{equation*}
- Find the $k$ that makes the probability of a match the largest.  

## The return of Bayes
- Use Bayes' theorem to find an expression for $\prob{Y = k | X = x}$:
  \begin{align*}
    \prob{Y = k | X = x} &= \frac{\prob{X = x | Y = k} \prob{Y = k}} {
    \sum_{k=1}^K \prob{X = x | Y = k} \prob{Y = k} } \\
    {} &\stackrel{model}{=} \frac{f_k(x) \pi_k}{f_1(x) \pi_1 + \ldots + f_K(x)
    \pi_K}.
  \end{align*}
- The **Bayes classifier** is the function $g$ defined by
  \begin{equation*}
    g(x) = \left\{ k \text{ that maximizes } \frac{f_k(x) \pi_k}{f_1(x) \pi_1 +
    \ldots + f_K(x) \pi_K} \right\}.
  \end{equation*}
- You can estimate $g$ by making assumptions about the $f_k$ and estimating the
  parameters of those sub-models.

## Everything is normal
- What if you assume that each $f_k$ is Gaussian?  Suppose there's just one $x$:
  \begin{equation*}
    f_k(x) = \frac{e^{-(x - \mu_k)^2 / 2\sigma^2}}{\sqrt{2\pi \sigma^2}}
  \end{equation*}
  for $k = 1, \ldots K$.  This assumes that $\sigma_k = \sigma$ for all $k$.
- Substitution and cancellation gives
  \begin{equation*}
    \prob{Y = k | X = x} = \frac{\pi_k e^{-(x - \mu_k)^2}}{\sum_k \pi_k e^{-(x -
    \mu_k)^2}}.
  \end{equation*}
- Even more mucking around (and dividing by $\pi_K f_k(x)$) gives, letting
  $d_k(x) = \alpha_k + \beta_k x$ for $\alpha_k$, $\beta_k$ complicated
  functions of the parameters,
  \begin{equation*}
    \prob{Y = k | X = x} = \frac{e^{d_k(x)}}{e^{d_1(x)} + e^{d_2(x)} + \ldots +
    e^{d_{K-1}(x)} + 1}.
  \end{equation*}

## Maximum likelihood: LDA and QDA
- The maximum likelihood estimate for the Gaussian model on the previous slide
  sets $\hat{\mu}_k = \bar{x}_k$ (average of $x_k$ in class $k$), $\hat{\pi}_k =
  n_k / n$ (proportion in class $k$) and $\hat{\sigma}^2 = \frac{1}{n} \sum_k
  \sum_{i: y_i = k} (x_i - \bar{x}_k)^2$.
- This is called the **linear discriminant analysis** (LDA) classifier, and it
  can be extended to accommodate more $x$.
- In case $\sigma_k$ is different over classes, the MLE plan can be extended to
  also estimate it (also covariances between variables), in what is known as the
  **quadratic discriminant analysis** (QDA) classifier.
- This leads to more complicated $\alpha_k$, $\beta_k$ in the $d_k$ functions,
  known as **discriminant functions**.
- These methods are discussed in section 4.4 of the ISLR book.

## Conditioning on $x$
- The full MLE plan requires that we think $(Y, X)$ are normally distributed.
- There are a number of models that have Bayes classifier that looks like
  \begin{equation*}
    g(x) = k \text{ that maximizes } \frac{e^{d_k(x)}}{ e^{d_1(x)} + \ldots +
    e^{d_{K-1}(x)} + 1}.
  \end{equation*}
- What if we just say that we would like to find the $k$ that maximizes $d_k(x)
  = \alpha_k + \beta_k x$?
- If we *condition* on the $x_1, \ldots, x_n$, then the likelihood is
  \begin{align*}
    \like(\theta | x_1, \ldots x_n) &=
    \prod_{i=1}^n \prob{Y = y_i | X = x_i} \\
    {} &= \prod_{k=1}^K \prod_{i: y_i = k} \frac{e^{d_k(x)}}{ e^{d_1(x)} + \ldots +
    e^{d_{K-1}(x)} + 1}.
  \end{align*}

## Setting up a logistic regression model
- By conditioning on $x$, it becomes sensible to model
  \begin{equation*}
    \prob{Y = k | X = x} := p_k(x, \beta) = \frac{e^{x\tr \beta_k}}{\sum_{k=1}^K
    e^{x\tr \beta_k}}
  \end{equation*}
  directly.
- This is a **multinomial** logit transformation, one with $K$ categories instead
  of 2.
- You usually need to divide by the $K$th category so that the MLE is
  well-defined, but we're about to penalize the likelihood, so it will be ok as
  it is.
- Also write 
  \begin{equation*}
  y_i = (0, \ldots, 1, \ldots, 0),
  \end{equation*}
  where the 1 is in the $k$th place.

## Multinomial logistic regression
- Like before, the deviance is
  \begin{equation*}
    D(\beta) = -2 \sum_{i=1}^n \sum_{k = 1}^K y_{ik} \log p_{k}(x_i, \beta_k).
  \end{equation*}
  Now $\beta$ has a separate $\beta_k$ vector for each class $k$.
- The (multinomial) logistic regression chooses $\beta$ to minimize the
  deviance, which can also be written
  \begin{align*}
    D(\beta) = -2 \sum_{i=1}^n \left( \sum_{k=1}^K y_{ik} x_i\tr \beta_k - \log
    \left( \sum_{k=1}^K e^{x_i\tr \beta_k} \right) \right).
  \end{align*}
  (Check it!)

## With a penalty, of course
- Now, for models with a very large number of covariates $x$, we will definitely
  want to penalize.  There is even a case to be made for models with a moderate
  number of $x$, because there are $K$ different coefficients for each new
  coordinate.
- The penalized objective function (that your computer can solve for you) is
  \begin{equation*}
    \min_\beta \left( \frac{-2}{n} \sum_{i=1}^n \sum_{k=1}^K y_{ik} \log
    p_k(x_i, \beta_k) + \lambda \sum_{k=1}^K \sum_{j=1}^p | \beta_{kj} | \right).
  \end{equation*}

## Estimation in R
- You can't solve the general multinomial problem by hand, even without a
  penalty.
- Regular multinomial logit estimation can be done using several R packages.
  We can do penalized estimation using `glmnet`.
- Here is an illustration using the glass data (try doing something similar for
  the iris data).  The covariates will be all the chemical variables interacted
  with the refractive index variable:

\scriptsize
```{r glmnet_est, warning=FALSE}
library(glmnet)
xfgl <- makeX(model.frame(type ~ . * RI, data = fgl)[, -1])
gtype <- fgl$type
glassfit <- cv.glmnet(xfgl, gtype, family = "multinomial")#, trace.it = TRUE)
```
\normalsize

## Lasso plot for each category

\scriptsize
```{r lasso, fig.height=6}
par(mfrow = c(2, 3), mar = c(3.1, 3.1, 2.1, 1.1), mgp = 2:0)
plot(glassfit$glm, xvar = "lambda")
```
\normalsize

## CV error

\scriptsize
```{r cv_error}
plot(glassfit) # across top avg # nonzero across classes
```
\normalsize

## Look at the coefficients
- Use the CV-minimizing choice for $\lambda$ (it's interesting to re-do the
  below steps with the 1-se rule to see the difference):

\scriptsize
```{r multicoef}
B  <- coef(glassfit, s = "lambda.min")
B <- do.call(cbind, B)
colnames(B) <- levels(gtype)
head(B)
```
\normalsize

- The coefficient matrix is $18 \times 6$ (there are also interactions).

## Predictions in terms of probabilities
- Now you can predict the classes.  The command below predicts in terms of
  `"response"`, so you see the estimated $\prob{Y_i = k | X_i}$ for each $i$ and
  $k$.

\scriptsize
```{r multipred}
probfgl <- predict(glassfit, xfgl, s = "lambda.min", type = "response")
probfgl <- drop(probfgl)
head(probfgl)
```
\normalsize

## Probabilities of true classes
- What probabilities were assigned to the true classes?

\scriptsize
```{r true_box, fig.height=6}
n <- nrow(xfgl)
trueclassprobs <- probfgl[cbind(1:n, gtype)]
# varwidth makes box widths proportional to response proportion.
plot(trueclassprobs ~ gtype, col = pal, varwidth = TRUE,
      xlab = "glass type", ylab = "prob( true class )")
```
\normalsize

## Correct predictions
- How do the Bayes classifier predictions do?  We can make a table to find out.

\scriptsize
```{r bayes_class}
yhat <- apply(probfgl, 1, which.max)
predict_tab <- table(yhat, gtype)
rownames(predict_tab) <- levels(gtype)[-3] # yhat is never 3
predict_tab
```
\normalsize

## Interpreting coefficients
- Reading multinomial logit coefficients can be weird.
- The key is to focus on the change in (log) odds between a pair of classes:
  \begin{equation*}
    \log (p_k / p_\ell) = \log \left( \frac{e^{x\tr \beta_k}}{e^{x\tr
    \beta_\ell}} \right) = x\tr (\beta_k - \beta_\ell).
  \end{equation*}
- For example:

```{r odd_compare}
exp(B["Mg", "WinNF"] - B["Mg", "WinF"])
exp(B["Mg", "WinNF"] - B["Mg", "Con"])
```

- For a unit increase in `Mg`, the odds of nonfloat glass over float glass
decrease by about 85%, but the odds of of nonfloat glass over container glass
increase by about 5 times!

## One difficulty
- Note that there isn't a simple way to state how $x$ affects the odds that $y =
  k$ against not $k$:
  \begin{align*}
    \log \left( \frac{p_k}{1 - p_k} \right) &= \log \left( \frac{p_k}{\sum_{\ell
    \neq k} p_\ell} \right) \\
    {} &= x\tr \beta_k - \log \left( \sum_{\ell \neq k} e^{x\tr \beta_\ell}
    \right).
  \end{align*}
  You'll have to just calculate what you want directly.
- Note that it isn't linear in $x$, so it can change for low/high $x$ values.

